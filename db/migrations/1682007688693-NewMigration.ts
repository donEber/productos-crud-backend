import { MigrationInterface, QueryRunner } from "typeorm";

export class NewMigration1682007688693 implements MigrationInterface {
    name = 'NewMigration1682007688693'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "product" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "quantity" integer NOT NULL, "price" integer NOT NULL, "date_registration" TIMESTAMP NOT NULL DEFAULT now(), "date_update" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_bebc9158e480b949565b4dc7a82" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "product"`);
    }

}
