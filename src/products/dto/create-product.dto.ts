export class CreateProductDto {
  name: string;
  quantity: number;
  price: number;
}
