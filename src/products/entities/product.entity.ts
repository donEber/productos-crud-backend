import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  quantity: number;
  @Column()
  price: number;
  @UpdateDateColumn({ type: 'timestamp' })
  date_registration: Date;
  @CreateDateColumn({ type: 'timestamp' })
  date_update: Date;
}
