
## Description

NestJS CRUD Products backend app

### Considerations

Run a postgreSQL database with:
- **database name**: `product-crud`
- **password**: "`123456`
- **user**: `postgres`
- **port**: `5432`

Before running the app, run the migrations with `npm run typeorm migration:run`

The backend runs in port 3001

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

Remember, the app runs in port 3001